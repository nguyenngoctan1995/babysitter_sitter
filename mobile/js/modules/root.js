import {combineEpics} from "redux-observable";
import {combineReducers} from "redux";

import {
	loadingAnimSplashEpic,
	loadingAnimSplashEpicSuccess
} from "../containers/SplashScreen/epic";

import {loadingSignInEpic} from "../containers/LoginScreen/epic";
import {loadingSignUpEpic} from "../containers/SignUpScreen/epic";
import {loadingResetEpic} from "../containers/ForgotPasswordScreen/epic";
import {loadingChangePasswordEpic} from "../containers/ChangePasswordScreen/epic";
import {loadingGetLeagueEpic, loadingGetUpcommingEventsEpic, loadingGetCreatedLeaguesEpic, loadingGetJoinedLeaguesEpic, loadingUpdateDeviceTokenEpic, loadingDeleteLeaguesEpic} from "../containers/HomeScreen/epic";
import {loadingGetNotificationEpic} from "../containers/NotificationScreen/epic";
import {loadingGetLeagueDetailEpic, loadingGetLeagueCategoriesEpic, loadingGetLeaguePlayersEpic, loadingJoinLeagueEpic, loadingCreateBallotEpic} from "../containers/LeagueDetailScreen/epic";
import {loadingGetEventCategoriesEpic, loadingGetEventLeagueEpic} from "../containers/EventDetailScreen/epic";
import {loadingCreateLeagueEpic, loadingGetUrlImgEpic, loadingUploadImgGoogleStorageEpic, loadingGetListEventsEpic, loadingGetCategoriesEventEpic} from "../containers/CreateLeagueScreen/epic";
import {loadingGetUserProfileEpic, loadingUploadUserProfilePictureEpic, loadingUploadImgGoogleEpic} from "../containers/ProfileScreen/epic";
import {loadingGetLeagueAllResultEpic, loadingGetLeagueMyFriendsResultEpic, loadingGetLeagueMyResultEpic, loadingGetUserBallotEpic} from "../containers/ResultScreen/epic";
import {loadingUpdateLeagueEpic} from "../containers/LeagueSettingScreen/epic";
import {loadingLogOutEpic} from "../containers/SettingScreen/epic";

import loginReducer from "../containers/LoginScreen/reducer";
import signUpReducer from "../containers/SignUpScreen/reducer";
import splashReducer from "../containers/SplashScreen/reducer";
import resetPasswordReducer from "../containers/ForgotPasswordScreen/reducer";
import changePasswordReducer from "../containers/ChangePasswordScreen/reducer";
import homeReducer from "../containers/HomeScreen/reducer";
import notificationReducer from "../containers/NotificationScreen/reducer";
import createLeagueReducer from "../containers/CreateLeagueScreen/reducer";
import updateLeagueReducer from "../containers/LeagueSettingScreen/reducer";
import leagueDetailReducer from "../containers/LeagueDetailScreen/reducer";
import eventDetailReducer from "../containers/EventDetailScreen/reducer";
import userProfileReducer from "../containers/ProfileScreen/reducer";
import resultReducer from "../containers/ResultScreen/reducer";
import logOutReducer from "../containers/SettingScreen/reducer";

/**
 * Combine all the epics into root
 */
export const rootEpic = combineEpics(
	loadingAnimSplashEpic,
	loadingAnimSplashEpicSuccess,
	loadingSignInEpic,
	loadingSignUpEpic,
	loadingResetEpic,
	loadingChangePasswordEpic,
	loadingGetLeagueEpic,
	loadingGetUpcommingEventsEpic,
	loadingGetCreatedLeaguesEpic,
	loadingGetJoinedLeaguesEpic,
	loadingGetNotificationEpic,
	loadingCreateLeagueEpic,
	loadingUpdateDeviceTokenEpic,
	loadingGetUrlImgEpic,
	loadingUploadImgGoogleStorageEpic,
	loadingGetListEventsEpic,
	loadingGetCategoriesEventEpic,
	loadingGetLeagueDetailEpic,
	loadingGetLeagueCategoriesEpic,
	loadingGetLeaguePlayersEpic,
	loadingJoinLeagueEpic,
	loadingDeleteLeaguesEpic,
	loadingGetEventCategoriesEpic,
	loadingGetEventLeagueEpic,
	loadingGetUserProfileEpic,
	loadingUploadUserProfilePictureEpic,
	loadingUploadImgGoogleEpic,
	loadingGetLeagueAllResultEpic,
	loadingGetLeagueMyResultEpic,
	loadingGetLeagueMyFriendsResultEpic,
	loadingGetUserBallotEpic,
	loadingCreateBallotEpic,
	loadingUpdateLeagueEpic,
	loadingLogOutEpic
);

/**
 * Combine all the reducers into root
 */
export const rootReducer = combineReducers({
	loginReducer,
	splashReducer,
	signUpReducer,
	resetPasswordReducer,
	changePasswordReducer,
	homeReducer,
	notificationReducer,
	createLeagueReducer,
	updateLeagueReducer,
	leagueDetailReducer,
	eventDetailReducer,
	userProfileReducer,
	resultReducer,
	logOutReducer
});
