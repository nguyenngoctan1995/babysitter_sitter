export default {
	/**
   * Define staging and production URL
   */
	URL: {
		urls: {
			staging: "https://staging-fanathon.inapps.technology:8443/api",
			production: "https://staging-fanathon.inapps.technology:8443/api" // For customer
		},
		socket: {
			url: "https://staging-fanathon.inapps.technology:8443",
			resultUpdated: "result_updated",
		},
		API_URL_PATH: {
			login: {
				url: "/auth/login",
				desc: "Login user name and password",
				params: [
					{
						type: "facebook|google|original",
						email: "string",
						password: "string",
						token: "string"
					}
				],
				method: "POST"
			},
			logout: {
				url: "/auth/logout",
				desc: "Logout",
				params: [
					{
						"registrationId": "string"
					  }
				],
				method: "POST"
			},
			reset_password: {
				url: "/auth/recoverPassword",
				desc: "Recover password via email",
				params: [
					{
						email: "string"
					}
				],
				method: "GET"
			},
			signup: {
				url: "/auth/register",
				desc: "Register new user name and password",
				params: [
					{
						email: "string",
						password: "string"
					}
				],
				method: "POST"
			},
			change_password: {
				url: "/auth/resetPassword",
				desc: "Change password",
				params: [
					{
						password: "string",
						token: "string"
					}
				],
				method: "POST"
			},
			eventUpcomming: {
				url: "/events/upcoming",
				desc: "Get upcomming events",
				params: [
					{
						"page": 1,
						"perPage": 10000
					}
				],
				method: "GET"
			},
			allEvents: {
				url: "/events",
				desc: "Get all events",
				params: [
					{

					}
				],
				method: "GET"
			},
			categoriesEvent: {
				url: "/categories?eventId=",
				desc: "Get categories of events",
				params: [
					{
						"eventId": 1,
					}
				],
				method: "GET"
			},
			notification: {
				changeDeviceToken: {
					url: "/users/me/notification",
					desc: "Change notification token",
					params: [
						{
							token: "string",
							os: "android||ios",
						}
					],
					method: "POST"
				}
			},
			league: {
				create: {
					url: "/leagues",
					desc: "Create new league",
					params: [
						{
							"name": "string",
							"image": "string",
							"status": "string",
							"detail": "string",
							"events": [
								{
									"id": "string",
									"start": "2019-08-28T02:03:26.055Z",
									"end": "2019-08-28T02:03:26.055Z"
								}
							]
						}
					],
					method: "POST"
				},
				createdLeague: {
					url: "/users/me/leagues",
					desc: "Get leagues created",
					params: [
						{
							"type": "created",
						}
					],
					method: "GET"
				},
				joinedLeague: {
					url: "/users/me/leagues",
					desc: "Get league joined",
					params: [
						{
							"type": "joined",
						}
					],
					method: "GET"
				},
				updateLeague: {
					url: "/leagues/{0}",
					desc: "Update new league",
					params: [
						{
							"name": "string",
							"image": "string",
							"status": "string",
							"detail": "string",
							"events": [
								{
									"id": "string",
									"start": "2019-08-28T02:03:26.055Z",
									"end": "2019-08-28T02:03:26.055Z"
								}
							]
						}
					],
					method: "PUT"
				},
				deleteLeague: {
					url: "/leagues/{0}",
					desc: "Delete league",
					params: [
						{

						}
					],
					method: "DELETE"
				},
				allLeague: {
					url: "/leagues/public",
					desc: "Get all leagues",
					params: [
						{

						}
					],
					method: "GET"
				},
				getUrlImageUploaded: {
					url: "/files/image",
					desc: "Get signed url for uploading to google storage",
					params: [
						{
							"filename": "file stype",
						}
					],
					method: "POST"
				},
				leagueDetail: {
					url: "/leagues/",
					desc: "Get detail league",
					params: [
						{

						}
					],
					method: "GET"
				},
				leagueCategories: {
					url: "/categories?leagueId={0}",
					desc: "Get detail league categories",
					params: [
						{

						}
					],
					method: "GET"
				},
				leaguePlayers: {
					url: "/players?leagueId={0}",
					desc: "Get detail league",
					params: [
						{

						}
					],
					method: "GET"
				},
				joinLeague: {
					url: "/leagues/{0}/join",
					desc: "Join league",
					params: [
						{
							"invitedBy": "string"
						}
					],
					method: "PUT"
				},
				results: {
					url: "/results?leagueId={0}&type=",
					desc: "Get results of league",
					params: [
						{
							"type": "all||me||friends",
						}
					],
					method: "GET"
				},
				ballots: {
					url: "/ballots?leagueId={0}&playerId=",
					desc: "Get ballot of player in league",
					params: [
						{

						}
					],
					method: "GET"
				}
			},
			ballot: {
				create: {
					url: "/ballots?leagueId={0}",
					desc: "Create new ballot",
					params: [
						{
							"leagueId": "string",
							"body": {
								"id": "string",
 								"ballots": [
									{
										"categoryId": "string",
										"eventLeagueId": "string",
										"items": [
											{
												"categoryNomineeId": "string",
												"rank": 0
											}
										]
									}
								]
							}
						}
					],
					method: "POST"
				}
			},
			event: {
				eventCategories: {
					url: "/categories?eventId={0}",
					desc: "Get event categories",
					params: [
						{

						}
					],
					method: "GET"
				},
				league: {
					url: "/leagues/public?eventId={0}",
					desc: "Get event categories",
					params: [
						{

						}
					],
					method: "GET"
				}
			},
			user: {
				getUserProfile: {
					url: "/users/me/profile",
					desc: "Get user profile",
					params: [
						{

						}
					],
					method: "GET"
				},
				updateProfilePicture: {
					url: "/users/me/profile",
					desc: "Get user profile",
					params: [
						{

						}
					],
					method: "PUT"
				}
			}
		}
	},
	PLATFORMS: {
		ANDROID: "android",
		IOS: "ios"
	},
	STATUS: {
		PRIVATE: "private",
		PROTECTED: "protected",
		PUBLIC: "public"
	},
	/**
   * Define methods for request apis
   */
	METHOD: {
		post: "POST",
		put: "PUT",
    get: "GET",
    delete: "DELETE"
	},
	LOGIN_TYPE: {
		facebook: "facebook",
		google: "google",
		original: "original"
	},
	/**
   * Set timeout request
   */
	TIME_OUT_REQUEST: 100000,

	/**
   * Constant for key/value in storage manager
   */
	USER: {
		USER_INFO: "USER_INFO",
		TOKEN_INFO: "TOKEN_INFO",
		DEVICE_TOKEN: "DEVICE_TOKEN",
	},
	NOTIFICATION: {
		VIBRATE_TIME: 500,
	},

	/**
   * Constant for mocking data
   */
	MOCKING_DATA: {
		USER: {
			USERNAME: "Admin@gmail.com",
			PASSWORD: "admin"
		},
		PLACE_HOLDER: "https://smithssanitationsupply.ca/wp-content/uploads/2018/06/noimage-1.png"
	},
	WEB_CLIENT_ID:
    "277276858297-u1gbl7jb8vib8co624tik5t26lpldc6e.apps.googleusercontent.com",
	LOCATION_BACKGROUND_BTN: [0, 0.36, 1],
	COLOR_BACKGROUND_BTN: ["#e2c85d", "#d9bf52", "#b89438"],
	CONFIG_IMAGE_PICKER: {
		title: "Select Profile picture",
		storageOptions: {
			quality: 0.8,
			maxHeight: 500,
			maxWidth: 500,
			storageOptions: {
				skipBackup: true,
			},
		},
  },
  FORBIDDEN: "Forbidden",
};
