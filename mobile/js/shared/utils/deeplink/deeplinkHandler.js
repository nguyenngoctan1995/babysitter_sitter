import { Linking } from "react-native";
import NavigationService from "../navigation/NavigationService";
import { ScreenNames } from "../../../route/ScreenNames";

const getUrlParameter = (url, name) => {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(url);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
};

export function deeplinkHandler({ url }) {
  if (url.includes("recoverPassword")) {
    const token = getUrlParameter(url.split("%20")[0], "token");
    return NavigationService.navigate(ScreenNames.ChangePasswordScreen, {
      token
    });
  }
  if (url.includes("league")) {
    const id = getUrlParameter(url, "id");
    const invitedBy = getUrlParameter(url, "invitedBy");
    return NavigationService.navigate(ScreenNames.LeagueDetailScreen, {
      invitedBy,
      item: { id },
    });
  }
}
