import {from, of} from "rxjs";
import {map, catchError, mergeMap} from "rxjs/operators";
import {ofType} from "redux-observable";
import * as constants from "./constants";
import {changePassword} from "./api";
import {changePasswordSuccess, changePasswordFailure} from "./actions";

export const loadingChangePassword = () => ({type: constants.CHANGE_PASSWORD});

export const loadingChangePasswordEpic = action$ => {
	return action$.pipe(
		ofType(constants.CHANGE_PASSWORD),
		mergeMap(action =>
			from(changePassword({password: action.payload.password, token: action.payload.token})).pipe(
				map(result => changePasswordSuccess({result})),
				catchError(error => of(changePasswordFailure({error})))
			)
		)
	);
};
