import React from "react";
import {View, Text, StatusBar} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import BaseScreen from "../BaseScreen/index";
import IAInput from "../../shared/components/IAInput";
import {styles} from "./style";
import {textStyles} from "../../shared/utils/styles/textStyles";
import {colors} from "../../shared/utils/colors/colors";
import Validator from "../../shared/utils/validator/Validator";
import I18n from "../../shared/utils/locale/i18n";
import {changePassword} from "./actions";
import IAHeader from "../../shared/components/IAHeader";
import LogManager from "../../shared/utils/logging/LogManager";
import Feather from "react-native-vector-icons/Feather";
import IAWavingView from "../../shared/components/IAWavingView";
import get from "lodash/get"

const langs = {
	changePasswordTitle: I18n.t("resetPasswordViaEmail.title"),
	newPassword: I18n.t("resetPasswordViaEmail.newPassword"),
	confirmPassword: I18n.t("resetPasswordViaEmail.confirmPassword"),
	forgotPassword: I18n.t("forgotPassword"),
	somethingWrong: I18n.t("somethingWrong"),
	error: I18n.t("error"),
	validEmailIncorrect: I18n.t("emailInvalid"),
	success: I18n.t("success"),
	resetPasswordSuccessfully: I18n.t("yourPasswordHasChanged"),
	mottoForgotPassword: I18n.t("mottoForgotPassword"),
	send: I18n.t("send"),
	forgotPasswordTitle: I18n.t("forgotPasswordTitle")
};

function ChangePasswordForm({editable, onChangeInput, username, password}) {
	return (
		<View style={[styles.formContainer]}>
			<IAInput
				label={langs.newPassword.toUpperCase()}
				activeColor={colors.black}
				underlineColor={colors.input_underline}
				labelStyle={textStyles.input_label}
				containerStyle={styles.inputEmail}
				editable={editable}
				keyboardType="email-address"
				returnKeyType="next"
				value={username}
				onChangeText={onChangeInput("newPassword")}
				secureTextEntry
			/>
			<IAInput
				label={langs.confirmPassword.toUpperCase()}
				activeColor={colors.black}
				underlineColor={colors.input_underline}
				labelStyle={textStyles.input_label}
				containerStyle={styles.inputPass}
				editable={editable}
				value={password}
				returnKeyType="done"
				onChangeText={onChangeInput("confirmPassword")}
				secureTextEntry
			/>
		</View>
	);
}

class ChangePasswordScreen extends BaseScreen {
	constructor(props) {
		super(props);
		this.state = {
			newPassword: "",
			confirmPassword: "",
		};
		this._onChangeInput = this._onChangeInput.bind(this);
		this.handleResponseChangePass = this.handleResponseChangePass.bind(this);
		this.validFormInput = this.validFormInput.bind(this);
		this._onResetPassword = this._onResetPassword.bind(this);
	}

	componentDidMount() {
		LogManager.showFullLog(LogManager.parseJsonObjectToJsonString(this.props));
	}

	componentWillReceiveProps(nextProps) {
		// handle response sign in on change
		if (nextProps.changePasswordStatus !== this.props.changePasswordStatus) this.handleResponseChangePass(nextProps);
	}

	async handleResponseChangePass(nextProps) {
		// not handle if the request is fetching
		if (nextProps.changePasswordStatus.isFetching()) return;
		// handle response
		if (nextProps.changePasswordStatus.isSuccess()) {
			this.alertInfo(langs.success, langs.resetPasswordSuccessfully, () => { this.goBack(); });
		}
		if (nextProps.changePasswordStatus.isFailure()) {
			// get error message from error object
			// const errorMessage =
			//   (nextProps.resetPasswordError && (await getErrorMessage(nextProps.resetPasswordError))) || "";
			this.alertInfo(langs.error, nextProps.changePasswordError.data.message);
		}
	}

	validFormInput() {
		const {newPassword, confirmPassword} = this.state;
		// check valid input
		let validEmptyNewPassword = "";
		validEmptyNewPassword = Validator.checkEmptyField(newPassword)
			? `${Validator.checkEmptyField(newPassword)}`
			: "";
		let validEmptyConfirmPassword = "";
		validEmptyConfirmPassword = Validator.checkEmptyField(confirmPassword)
			? `${Validator.checkEmptyField(confirmPassword)}`
			: "";
		const validMatchPass = Validator.checkConfirmPassword(newPassword, confirmPassword);

		const isInvalid = validEmptyNewPassword || validEmptyConfirmPassword || validMatchPass;

		// get error message
		var errorMessage = "";
		if (validEmptyNewPassword != "") {
			errorMessage = `${validEmptyNewPassword}`;
		} else if (validEmptyConfirmPassword != ""){
			errorMessage = `${validEmptyConfirmPassword}`;
		} else if (validMatchPass != "") {
			errorMessage = `${validMatchPass}`;
		}
		// return isInvalid and message
		return {isInvalid, errorMessage};
	}

	_onResetPassword() {
		const {token} = this.props
		if (this.props.changePasswordStatus.isFetching()) return;

		const {newPassword} = this.state;
		const {isInvalid, errorMessage} = this.validFormInput();
		if (isInvalid) {
			this.alertInfo(langs.error, errorMessage);
			return;
		}
		// Change password process
		this.dismissKeyboard();
		this.props.changePassword({token, password: newPassword});
	}

	onChangeInput(key) {
		return value => this.setState({[key]: value});
	}

	_onChangeInput(key) {
		return value => this.setState({[key]: value});
	}

	renderCheck() {
		const {changePasswordStatus} = this.props;
		const btnLoading = changePasswordStatus.isFetching();
		return (
			<View>
				{btnLoading ?
					<View>
						<IAWavingView size={10}/>
					</View>
					:
					<Feather name={"check"} size={32} color={colors.yellow}/>
				}
			</View>
		);
	}

	render() {
		const {username} = this.state;
		const {changePasswordStatus} = this.props;

		return (
			<View style={styles.mainContainer}>
				<StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
				<IAHeader viewLeft={this.renderBackButton()}
					viewRight={this.renderCheck()}
					styleLeft={styles.headerLeft}
					onPressRight={()=>{this._onResetPassword();}}
					onPressLeft={()=>this.goBack()}/>
				<View style={styles.mainContent}>
					<Text style={[styles.welcomeTitle]}>{langs.changePasswordTitle.toUpperCase()}</Text>
					<ChangePasswordForm
						editable={!changePasswordStatus.isFetching()}
						onChangeInput={this._onChangeInput}
						username={username}/>
				</View>
			</View>
		);
	}
}
const mapStateToProps = (state, ownProps) => {
	return {
		token: get(ownProps, 'navigation.state.params.token', ""),
		changePasswordStatus: state.changePasswordReducer.changePassword.status,
		changePasswordError: state.changePasswordReducer.changePassword.error,
	};
};
const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			changePassword
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ChangePasswordScreen);
