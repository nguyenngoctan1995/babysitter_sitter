import Constant from "../../shared/utils/constant/Constant";
import {AxiosFetch} from "../../api/AxiosFetch";

export function changePassword({password, token}) {
	return new Promise((rs, rj) => {
		try {
			AxiosFetch({
				method: "POST",
				url: Constant.URL.API_URL_PATH.change_password.url,
				data: {"password": password, "token": token},
				onSuccess: data => {
					rs(data.result);
				},
				onError: error => {
					rj(error);
				}
			});
		// eslint-disable-next-line no-empty
		} catch (error) {
		}
	});
}

export default {
	changePassword
};
