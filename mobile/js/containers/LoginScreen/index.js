import React from "react";
import {View, Text, TouchableOpacity, StatusBar, ScrollView} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import BaseScreen from "../BaseScreen/index";
import IAInput from "../../shared/components/IAInput";
import {styles} from "./style";
import {textStyles} from "../../shared/utils/styles/textStyles";
import {colors} from "../../shared/utils/colors/colors";
import Validator from "../../shared/utils/validator/Validator";
import I18n from "../../shared/utils/locale/i18n";
import {signIn} from "./actions";
import {ScreenNames} from "../../route/ScreenNames";
import IAHeader from "../../shared/components/IAHeader";
import LinearGradient from "react-native-linear-gradient";
import {LoginManager} from "react-native-fbsdk";
import icons from "../../shared/utils/icons/icons";
import {AccessToken, GraphRequest, GraphRequestManager} from "react-native-fbsdk";
import {GoogleSignin, statusCodes} from "react-native-google-signin";
import IALocalStorage from "../../shared/utils/storage/IALocalStorage";
import IARefreshing from "../../shared/components/IARefreshing";
import Constant from "../../shared/utils/constant/Constant";
import IAText from "../../shared/components/IAText";
import LogManager from "../../shared/utils/logging/LogManager";

const langs = {
	welcomeBack: I18n.t("welcomeTitle"),
	signInTitle: I18n.t("signInTitle"),
	email: I18n.t("email"),
	password: I18n.t("password"),
	forgotPassword: I18n.t("forgotPassword"),
	signIn: I18n.t("signIn"),
	btnLoginFacebook: I18n.t("fb"),
	somethingWrong: I18n.t("somethingWrong"),
	error: I18n.t("error"),
	validEmailIncorrect: I18n.t("emailInvalid"),
	validPasswordIncorrect: I18n.t("passwordInvalid"),
	success: I18n.t("success"),
	loginSuccess: I18n.t("loginSuccess"),
	loginFailedTitle: I18n.t("loginFail"),
	googleNotSupport: I18n.t("notSupport"),
	googleSignInError: I18n.t("googleSignInSuspend"),
	errorFetchingDataFB: I18n.t("errorFetchingData"),
};
const EMAIL = "email";
const PASSWORD = "password";

function LoginForm({editable, onChangeInput, email, password}) {
	return (
		<View style={[styles.formContainer]}>
			<IAInput
				label={langs.email.toUpperCase()}
				activeColor={colors.black}
				underlineColor={colors.input_underline}
				labelStyle={textStyles.input_label}
				containerStyle={styles.inputEmail}
				editable={editable}
				keyboardType="email-address"
				value={email}
				returnKeyType="next"
				onChangeText={onChangeInput(EMAIL)}
			/>
			<IAInput
				label={langs.password.toUpperCase()}
				activeColor={colors.black}
				secureTextEntry
				underlineColor={colors.input_underline}
				labelStyle={textStyles.input_label}
				containerStyle={styles.inputPass}
				editable={editable}
				value={password}
				returnKeyType="done"
				onChangeText={onChangeInput(PASSWORD)}
			/>
		</View>
	);
}

/**
 * This should be modified a little bit that if user has logged in, directly go to homescreen
 * This will be belong to next sprint
 */
class LoginScreen extends BaseScreen {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: ""
		};
		this._onChangeInput = this._onChangeInput.bind(this);
		this.handleResponseSignIn = this.handleResponseSignIn.bind(this);
		this.validFormInput = this.validFormInput.bind(this);
		this._goToLoginFacebook = this._goToLoginFacebook.bind(this);
		this._onLoginPressed = this._onLoginPressed.bind(this);
		this._goToForgotPassword = this._goToForgotPassword.bind(this);
	}

	componentWillMount() {
		// Configuration for google signin flow
		GoogleSignin.configure({
			webClientId: Constant.WEB_CLIENT_ID
		});
	}

	componentWillReceiveProps(nextProps) {
		// handle response sign in on change
		if (nextProps.signInStatus !== this.props.signInStatus) this.handleResponseSignIn(nextProps);
	}

	async handleResponseSignIn(nextProps) {
		if (nextProps.signInStatus.isFetching()) return;
		// handle response
		if (nextProps.signInStatus.isSuccess()) {
			await this._saveUserInfo(nextProps.signInUserInfo);
			this._goToHomeScreen();
			return;
		}
		if (nextProps.signInStatus.isFailure()) {
			// get error message from error object
			// const errorMessage =
			//   (nextProps.signInError && (await getErrorMessage(nextProps.signInError))) || "";
			this.alertInfo(langs.loginFailedTitle, nextProps.signInError.message ? nextProps.signInError.message : (nextProps.signInError.data ? nextProps.signInError.data.message : langs.error));
			return;
		}
	}

	_saveUserInfo(userInfo) {
		IALocalStorage.setTokenUserInfo(userInfo.token);
		IALocalStorage.setUserInfo(userInfo);
	}

  _goToHomeScreen = () => {
  	this.goToScreen(ScreenNames.HomeScreen);
  }

  validFormInput() {
  	const {email, password} = this.state;
  	// check valid input
  	let validUserName = "";
  	validUserName = Validator.checkEmptyField(email)
  		? `${Validator.checkEmptyField(email)}`
  		: "";
  	const validPassword = Validator.checkEmptyField(password)
  		? `\n${Validator.checkEmptyField(password)}`
  		: "";
  	const validUsername = Validator.checkEmail(email)
  		? `\n${Validator.checkEmail(email)}`
  		: "";
  	const isInvalid = validUserName || validUsername || validPassword;
  	// get error message
  	var errorMessage = "";
  	if (validUserName != "") {
  		errorMessage = `${validUserName}`;
  	} else {
  		errorMessage = `${validUsername} ${validPassword}`;
  	}

  	// return isInvalid and message
  	return {isInvalid, errorMessage};
  }

  _onLoginPressed() {
  	this.signOut();
  	const {email, password} = this.state;
  	const {isInvalid, errorMessage} = this.validFormInput();
  	if (isInvalid) {
  		this.alertInfo(langs.error, errorMessage);
  		return;
  	}

  	// Sign in process
  	this.dismissKeyboard();
  	this.props.signIn({email,
  		password,
  		type: Constant.LOGIN_TYPE.original,
  		token: ""});
  }

  _onChangeInput(key) {
  	if (key === PASSWORD) {
  		return value => this.setState({[key]: value});
  	}
  	return value => this.setState({[key]: Validator.trimValue(value)});
  }

  _goToLoginFacebook() {
	  LoginManager.logInWithPermissions(["public_profile", "email"])
	  .then(
  		result => {
  			if (!result.isCancelled) {
  				this._getFBUserInfo();
  			}
  		},
  		() => {
  			this.alertInfo(langs.error, langs.somethingWrong);
  		}
  	).catch(err => {
		  console.log(LogManager.parseJsonObjectToJsonString(err))
	  });
  }

  _getFBUserInfo() {
  	AccessToken.getCurrentAccessToken()
  		.then((user) => {
  			// There are some info here:
  			// user.accessToken
  			// user.accessTokenSource
  			// user.userID
  			IALocalStorage.setTokenUserInfo(user.accessToken);
  			return user;
  		})
  		.then((user) => {
  			const responseInfoCallback = (error, result) => {
  				if (error) {
  					this.alertInfo(langs.errorFetchingDataFB, error.toString());
  				} else {
  					// There are some info here:
  					// email, id, last_name, first_name, name, picture.
  					// They are exist here because of depending on the fields in GraphRequest
  					IALocalStorage.setUserInfo(result);
  					this.props.signIn({email: result.email,
  						password: "",
  						type: Constant.LOGIN_TYPE.facebook,
  						token: user.accessToken});
  				}
  			};
  			// Config request for getting information
  			const infoRequest = new GraphRequest("/me", {
  				accessToken: user.accessToken,
  				parameters: {
  					fields: {
  						string: "email, name, first_name, last_name, picture"
  					}
  				}
  			}, responseInfoCallback);

  			// Start the graph request.
  			new GraphRequestManager()
  				.addRequest(infoRequest)
  				.start();
  		});
  }

  _renderLoginBtn() {
  	const {signInStatus} = this.props;
  	const btnLoading = signInStatus.isFetching();
  	return (
  		<View style={styles.btnLoginContainer}>
  			{ btnLoading ?
  				<View style={styles.btnLogin}>
  					<IARefreshing />
  				</View>
  				:
  				<TouchableOpacity style={styles.btnLogin} onPress={this._onLoginPressed}>
  					<LinearGradient
  						colors={Constant.COLOR_BACKGROUND_BTN}
  						angle= {1}
  						start={{x: 0, y: 0}}
  						end={{x: 1, y: 0}}
  						location={Constant.LOCATION_BACKGROUND_BTN}
  						style={styles.buttonSignInContainer}>
  						<Text style={styles.buttonText}>
  							{langs.signIn.toUpperCase()}
  						</Text>
  					</LinearGradient>
  				</TouchableOpacity>
  			}
  		</View>
  	);
  }

  signIn = async () => {
  	try {
  		await GoogleSignin.hasPlayServices();
  		const loggedInUser = await GoogleSignin.signIn();
  		const token  = await GoogleSignin.getTokens();
  		// Login with google
  		this.props.signIn({email: loggedInUser.user.email,
  			password: "",
  			type: Constant.LOGIN_TYPE.google,
  			token: token.idToken});

  	} catch (error) {
  		this.handleSignInError(error);
  	}
  };

  /**
   * @name handleSignInError
   * @param error the SignIn error object
   */
  handleSignInError = async error => {
  	if (error.code) {
  		if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  			// this.showSignInError('User cancelled the login flow.');
  		} else if (error.code === statusCodes.IN_PROGRESS) {
  			// this.showSignInError('Sign in is in progress.');
  		} else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
  			await this.getGooglePlayServices();
  		} else {
  			console.log(error);
  			// this.alertInfo(langs.error, JSON.stringify(error))
  		}
  	} else {
  		console.log(error);
  		// this.alertInfo(langs.error, JSON.stringify(error))
  	}
  };

  /**
   * @name signOut
   */
  signOut = async () => {
  	try {
  		await GoogleSignin.revokeAccess();
  		await GoogleSignin.signOut();
  	} catch (error) {
  		this.handleSignInError(error);
  	}
  };

  getGooglePlayServices = async () => {
  	try {
  		await GoogleSignin.hasPlayServices({
  			showPlayServicesUpdateDialog: true
  		});
  		// Google services are available
  	} catch (err) {
  		this.alertInfo(langs.error, langs.googleNotSupport);
  	}
  };
  /**
   * @name isUserSignedIn
   */
  isUserSignedIn = async () => {
  	this.setState({isUserSignedIn: false, checkingSignedInStatus: true});
  	const isUserSignedIn = await GoogleSignin.isSignedIn();
  	if (isUserSignedIn) {
  		await this.getCurrentUserInfo();
  	}
  	this.setState({isUserSignedIn, checkingSignedInStatus: false});
  };

  /**
   * @name getCurrentUserInfo
   */
  getCurrentUserInfo = async () => {
  	try {
  		const loggedInUser = await GoogleSignin.signInSilently();
  		this.setState({loggedInUser});
  	} catch (error) {
  		this.setState({loggedInUser: {}});
  	}
  };

  _goToForgotPassword() {
  	this.goToScreen(ScreenNames.ForgotPasswordScreen);
  }

  _renderFBSDKLogin() {
  	return (
  		<TouchableOpacity style={[styles.fbContainer]} onPress={this._goToLoginFacebook}>
  			<View style={styles.btnFBLoginContainer}>
  				<IAText text="f" style={styles.fbText}/>
  			</View>
  		</TouchableOpacity>
  	);
  }

  _renderGoogleLogin() {
  	return (
  		<TouchableOpacity style={styles.googleContainer} onPress={this.signIn}>
  			<View style={styles.btnGoogleLoginContainer}>
  				{icons.google.IC_GOOGLE}
  			</View>
  		</TouchableOpacity>
  	);
  }

  _renderForgotPassword() {
  	return (
  		<View style={styles.forgotPassword} >
  			<TouchableOpacity onPress={this._goToForgotPassword}>
  				<Text style={[styles.signInTitle]}>{langs.forgotPassword}</Text>
  			</TouchableOpacity>
  		</View>
  	);
  }

  render() {
  	const {email, password} = this.state;
  	return (
  		<View style={styles.mainContainer}>
  			<StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
  			<View style={{marginTop: 11}}>
  				<IAHeader viewLeft={this.renderBackButton()}
  					styleLeft={styles.headerLeft}
  					onPressLeft={()=>this.goBack()}/>
  			</View>
  			<ScrollView style={styles.mainContainer}>
  				<View style={styles.mainContent}>
  					<Text style={[styles.welcomeTitle]}>{langs.welcomeBack.toUpperCase()}</Text>
  					<Text style={[styles.signInTitle]}>{langs.signInTitle}</Text>
  					<LoginForm
  						editable={true}
  						onChangeInput={this._onChangeInput}
  						email={email}
  						password={password}/>
  					{this._renderForgotPassword()}
  					{this._renderLoginBtn()}
  					<View style={styles.containerLoginSocial}>
  						{this._renderFBSDKLogin()}
  						{this._renderGoogleLogin()}
  					</View>

  				</View>
  			</ScrollView>
  		</View>
  	);
  }
}
const mapStateToProps = state => {
	return {
		signInStatus: state.loginReducer.signIn.status,
		signInError: state.loginReducer.signIn.error,
		signInUserInfo: state.loginReducer.signIn.userInfo,
	};
};
const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			signIn
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginScreen);
