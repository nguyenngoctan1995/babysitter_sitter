import Constant from "../../shared/utils/constant/Constant";
import {AxiosFetch} from "../../api/AxiosFetch";

export function signUp({email, password}) {
	return new Promise((rs, rj) => {
		try {
			AxiosFetch({
				method: "POST",
				url: Constant.URL.API_URL_PATH.signup.url,
				data: {"email": email, "password": password},
				onSuccess: data => {
					rs(data.result);
				},
				onError: error => {
					rj(error);
				}
			});
		// eslint-disable-next-line no-empty
		} catch (error) {
		}
	});
}

export default {
	signUp
};
