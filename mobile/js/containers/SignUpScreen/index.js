import React from "react";
import {View, Text, TouchableOpacity, StatusBar} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import BaseScreen from "../BaseScreen/index";
import IAInput from "../../shared/components/IAInput";
import {styles} from "./style";
import {textStyles} from "../../shared/utils/styles/textStyles";
import {colors} from "../../shared/utils/colors/colors";
import Validator from "../../shared/utils/validator/Validator";
import I18n from "../../shared/utils/locale/i18n";
import {signUp} from "./actions";
import IAHeader from "../../shared/components/IAHeader";
import LinearGradient from "react-native-linear-gradient";
import icons from "../../shared/utils/icons/icons";
import IARefreshing from "../../shared/components/IARefreshing";
import CheckBox from "react-native-check-box";
import Constant from "../../shared/utils/constant/Constant";

const langs = {
	signUpTitle: I18n.t("signUpTitle"),
	email: I18n.t("email"),
	password: I18n.t("password"),
	forgotPassword: I18n.t("forgotPassword"),
	signUp: I18n.t("signUp"),
	somethingWrong: I18n.t("somethingWrong"),
	error: I18n.t("error"),
	validEmailIncorrect: I18n.t("emailInvalid"),
	validPasswordIncorrect: I18n.t("passwordInvalid"),
	success: I18n.t("success"),
	signUpSuccess: I18n.t("signUpSuccess"),
	mottoSignUp: I18n.t("mottoSignUp"),
	termAndCondition: I18n.t("termAndCondition"),
	forceTermAndCondition: I18n.t("forceTermAndCondition")
};
const EMAIL = "email";
const PASSWORD = "password";

function SignUpForm({editable, onChangeInput, email, password}) {
	return (
		<View style={[styles.formContainer]}>
			<IAInput
				label={langs.email.toUpperCase()}
				activeColor={colors.black}
				underlineColor={colors.input_underline}
				labelStyle={textStyles.input_label}
				containerStyle={styles.inputEmail}
				editable={editable}
				keyboardType="email-address"
				returnKeyType="next"
				value={email}
				onChangeText={onChangeInput(EMAIL)}
			/>
			<IAInput
				label={langs.password.toUpperCase()}
				activeColor={colors.black}
				secureTextEntry
				refInner={PASSWORD}
				underlineColor={colors.input_underline}
				labelStyle={textStyles.input_label}
				containerStyle={styles.inputPass}
				editable={editable}
				value={password}
				returnKeyType="done"
				onChangeText={onChangeInput(PASSWORD)}
			/>
		</View>
	);
}


class SignUpScreen extends BaseScreen {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: "",
			isChecked: false,
		};
		this._onChangeInput = this._onChangeInput.bind(this);
		this.handleResponseSignUp = this.handleResponseSignUp.bind(this);
		this.validFormInput = this.validFormInput.bind(this);
		this._onSignUpPressed = this._onSignUpPressed.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		// handle response sign in on change
		if (nextProps.signUpStatus !== this.props.signUpStatus) this.handleResponseSignUp(nextProps);
	}

	async handleResponseSignUp(nextProps) {
		// not handle if the request is fetching
		if (nextProps.signUpStatus.isFetching()) return;
		// handle response
		if (nextProps.signUpStatus.isSuccess()) {
			// this.goToScreen(ScreenNames.MainTabScreen);
			this.alertInfo(langs.success, langs.signUpSuccess, () => this.goBack());
		}
		if (nextProps.signUpStatus.isFailure()) {
			// get error message from error object
			// const errorMessage =
			//   (nextProps.signUpError && (await getErrorMessage(nextProps.signUpError))) || "";
			this.alertInfo(langs.loginFailedTitle, nextProps.signUpError.data.message);
		}
	}

	validFormInput() {
		const {email, password} = this.state;
		// check valid input
		let validUserName = "";
		validUserName = Validator.checkEmptyField(email)
			? `${Validator.checkEmptyField(email)}`
			: "";
		const validPassword = Validator.checkEmptyField(password)
			? `\n${Validator.checkEmptyField(password)}`
			: "";
		const validUsername = Validator.checkEmail(email)
			? `\n${Validator.checkEmail(email)}`
			: "";
		const isInvalid = validUserName || validPassword;
		// get error message
		var errorMessage = "";
		if (validUserName != "") {
			errorMessage = `${validUserName}`;
		} else {
			errorMessage = `${validUsername} ${validPassword}`;
		}

		// return isInvalid and message
		return {isInvalid, errorMessage};
	}

	_onSignUpPressed() {
		const {email, password} = this.state;
		const {isInvalid, errorMessage} = this.validFormInput();
		if (isInvalid) {
			this.alertInfo(langs.error, errorMessage);
			return;
		}
		if (!this.state.isChecked && !isInvalid) {
			this.alertInfo(langs.error, langs.forceTermAndCondition);
			return;
		}
		// Sign up process
		this.dismissKeyboard();
		this.props.signUp({email: email, password: password});
	}

	_onChangeInput(key) {
		if (key === PASSWORD) {
			return value => this.setState({[key]: value});
		}
		return value => this.setState({[key]: Validator.trimValue(value)});
	}

	_renderSignUpBtn() {
		const {signUpStatus} = this.props;
		const btnLoading = signUpStatus.isFetching();
		return (
			<View style={styles.btnLoginContainer}>
				{btnLoading ?
					<View style={styles.btnLogin}>
						<IARefreshing />
					</View>
					:
					<TouchableOpacity style={styles.btnLogin} onPress={this._onSignUpPressed}>
						<LinearGradient
							colors={Constant.COLOR_BACKGROUND_BTN}
							angle= {1}
							start={{x: 0, y: 0}}
							end={{x: 1, y: 0}}
							location={Constant.LOCATION_BACKGROUND_BTN}
							style={styles.buttonSignInContainer}>
							<Text style={styles.buttonText}>
								{langs.signUp.toUpperCase()}
							</Text>
						</LinearGradient>
					</TouchableOpacity>
				}
			</View>
		);
	}

	_renderChecked() {
		return (
			<View style={styles.checkBox}>
				{icons.checkbox.checked}
			</View>
		);
	}

	_renderUnCheck() {
		return (
			<View style={styles.checkBox}>
			</View>
		);
	}

	_renderTermAndCondition() {
		return (
			<View style={styles.termConditionContainer}>
				<View style={{marginEnd: 11}}>
					<CheckBox
						style={styles.checkBoxContainer}
						onClick={()=>{
							this._toggleCheckbox();
						}}
						isChecked={this.state.isChecked}
						checkedImage={this._renderChecked()}
						unCheckedImage={this._renderUnCheck()}/>
				</View>
				<TouchableOpacity onPress={()=>this._toggleCheckbox()}>
					<Text style={styles.termAndCondition}>{langs.termAndCondition}</Text>
				</TouchableOpacity>
			</View>
		);
	}

  _toggleCheckbox = () => {
  	this.setState({
  		isChecked:!this.state.isChecked
  	});
  }

  render() {
  	const {email, password} = this.state;
  	const {signUpStatus} = this.props;
  	const btnLoading = signUpStatus.isFetching();

  	return (
  		<View style={styles.mainContainer}>
  			<StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
  			<IAHeader viewLeft={this.renderBackButton()}
  				styleLeft={styles.headerLeft}
  				onPressLeft={()=>this.goBack()}/>
  			<View style={styles.mainContent}>
  				<Text style={[styles.welcomeTitle]}>{langs.signUpTitle.toUpperCase()}</Text>
  				<Text style={[styles.signInTitle]}>{langs.mottoSignUp}</Text>
  				<SignUpForm
  					editable={!btnLoading}
  					onChangeInput={this._onChangeInput}
  					email={email}
  					password={password}/>
  				{ this._renderTermAndCondition() }
  				{ this._renderSignUpBtn() }

  			</View>
  		</View>
  	);
  }
}
const mapStateToProps = state => {
	return {
		signUpStatus: state.signUpReducer.signUp.status,
		signUpError: state.signUpReducer.signUp.error,
	};
};
const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			signUp
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SignUpScreen);
