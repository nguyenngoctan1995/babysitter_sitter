import React from "react";
import {View, ScrollView, TouchableOpacity, Animated, Image, ImageBackground, StatusBar, RefreshControl, NetInfo, Platform} from "react-native";
import BaseScreen from "../BaseScreen/index";
import {styles} from "./style";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import I18n from "../../shared/utils/locale/i18n";
import {DrawerActions} from "react-navigation";
import {ScreenNames} from "../../route/ScreenNames";
import IAHeader from "../../shared/components/IAHeader";
import IAText from "../../shared/components/IAText";
import IALocalStorage from "../../shared/utils/storage/IALocalStorage";
import icons from "../../shared/utils/icons/icons";
import FastImage from "react-native-fast-image";
import Constant from "../../shared/utils/constant/Constant";
import {images} from "../../../assets";
import * as Animatable from "react-native-animatable";
import {colors} from "../../shared/utils/colors/colors";
import ImagePicker from "react-native-image-picker";
import LogManager from "../../shared/utils/logging/LogManager";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import LinearGradient from "react-native-linear-gradient";
import {getUserProfile, uploadImgGoogle, uploadUserProfilePicture} from "./actions";
import {addUserDataListener, removeUserDataListener} from "./socket";
import debounce from "lodash/debounce";

const langs = {
	title: I18n.t("myProfile.title"),
	empty: I18n.t("empty"),
	noLeague: I18n.t("myProfile.noLeague"),
	joinLeague: I18n.t("myProfile.joinLeague"),
	totalPoints: I18n.t("myProfile.totalPoints"),
	myLeague: I18n.t("home.myLeagues"),
	success: I18n.t("success")
};

class ProfileScreen extends BaseScreen {
	constructor(props) {
		super(props);
		this.state = {
			tab: langs.allEvents,
			marginAll: 10,
			currentLeagueId: 0,
			currentImgChosen: "",
			userInfo: {},
			shouldUpdate: false
		};
	}

	addTriggerForScreen() {
        this.screenFocusListener = this.props && this.props.navigation.addListener('willFocus', () => { 
			this._getUserInfo();
			addUserDataListener(debounce(this.handleUserDataChange), 500);
			this.connectionListener = NetInfo.addEventListener("connectionChange", this.handleConnectionChange);
        });
    }

    removeTrigger() {
        this.screenFocusListener.remove();
		this.connectionListener.remove();
        removeUserDataListener();
	}
	
	handleUserDataChange = async (users = []) => {
		const currentUser = await IALocalStorage.getUserInfo();
		if (!currentUser || currentUser == "") return;
		if(users.length > 0 && users.includes(currentUser.id))
			return this._getUserInfo();
	}

	handleConnectionChange = ({type}) => {
        if(type == "wifi" || type == "cellular"){
            return addUserDataListener(debounce(this.handleUserDataChange), 500);
        }
        return removeUserDataListener();
    }

    componentDidMount() {
		this._getUserInfo();
        this.addTriggerForScreen();
    }

    componentWillUnmount() {
        this.removeTrigger();
    }

	componentWillReceiveProps(nextProps) {
			if (nextProps.userProfileStatus.isFetching() || 
					nextProps.uploadImgGoogleStorageStatus.isFetching()) return;
			if (nextProps.uploadImgGoogleStorageStatus.isSuccess()) {
				var imgUrl = nextProps.uploadImgGoogleStorage
				if (imgUrl) {
					this.setState({currentImgChosen: imgUrl, shouldUpdate: true}, () =>{})
				}
			}
			if (nextProps.userProfileUpdateStatus.isSuccess()) {
				this.alertInfo(langs.success, nextProps.userProfileError)
			}
	}

	_updateProfilePicture = (imgUrl) => {
		this.props.uploadUserProfilePicture({filename: imgUrl});
	}

	_getUserInfo() {
		IALocalStorage.getUserInfo()
			.then(val => {
				if (val && val != "") {
					this.setState({userInfo: val});
				}
			});
		this.props.getUserProfile();
	}

	_renderDrawerIco() {
  	return (
  		<TouchableOpacity onPress={()=>this._openDrawer()}>
  			<View style={styles.drawer}>
  				{ icons.drawer.IC_DRAWER_WHITE }
  			</View>
  		</TouchableOpacity>
  	);
	}

	_renderSetting() {
  	return (
  		<TouchableOpacity onPress={()=>this.goToScreen(ScreenNames.SettingScreen)}>
  			<View>
  				{ icons.profile.IC_PROFILE_SETTING }
  			</View>
  		</TouchableOpacity>
  	);
	}

	_openDrawer() {
		this.props.navigation.dispatch(DrawerActions.openDrawer());
	}

	_onRefresh = () => {
		this._getUserInfo();
	}

	_renderProfilePicture = () => {
		var img = this.state.currentImgChosen != "" ? this.state.currentImgChosen : (this.props.userProfile.avatar ? this.props.userProfile.avatar   : Constant.MOCKING_DATA.PLACE_HOLDER);
		return (
			<TouchableOpacity onPress={()=>{this._openGalleryOrCamera();}} style={[styles.profilePicture, styles.shadow]}>
				<View style={[styles.uploadImageContainer, styles.shadow]}>
					{ this.state.currentImgChosen != "" ?
						<FastImage
							style={styles.containerSquare}
							source={{
								uri: img,
								priority: FastImage.priority.high,
							}}
							resizeMode={FastImage.resizeMode.cover}/>
						:
						<View>
							<Image source={{uri: img}} style={styles.mock} resizeMode="cover" resizeMethod="resize"/>
						</View> }
				</View>
				<View style={[styles.camera, styles.shadow]}>
					<EvilIcons name="camera" size={23} color={colors.yellow_camera} style={{alignSelf: "center"}}/>
				</View>
			</TouchableOpacity>
		);
	}

	_openGalleryOrCamera = () => {
		ImagePicker.showImagePicker(Constant.CONFIG_IMAGE_PICKER.storageOptions, (response) => {
			if (response.didCancel) {
				console.log("User cancelled image picker");
			} else if (response.error) {
				console.log("ImagePicker Error: ", response.error);
			} else if (response.customButton) {
				console.log("User tapped custom button: ", response.customButton);
			} else {
				let source = response.uri;
				let imageItem = {image: source, data: response};
				console.log(LogManager.parseJsonObjectToJsonString(imageItem.image));
				this.setState({currentImgChosen: imageItem.image, shouldUpdate: false});
				this.props.uploadImgGoogle({filename: imageItem});
			}
		});
	}

	_renderWave = () => {
		return (
			<Animatable.Image animation="pulse" iterationCount="infinite" source={images.wave}
				style={styles.waveContainer}/>
		);
	}

	_renderBasicUserInfo () {
		var userInfo = this.props.userProfile.id ? this.props.userProfile : this.state.userInfo;
		var userName = userInfo.firstName && userInfo.firstName;
		var email = userInfo.email && userInfo.email;
		if (userName == "" || !userName) {
			userName = langs.empty;
		}
		return (
			<Animatable.View animation="fadeInUpBig" duration={600}>
				<IAText text={userName} style={styles.name}/>
				<IAText text={email} style={styles.email}/>
			</Animatable.View>
		);
	}

	_renderTotalPoints = () => {
		const point = this.props.userProfile.points || 0;
		return (
			<Animatable.View animation="bounceInDown" duration={700} style={styles.totalPointContainer}>
				<View style={styles.totalPointBound}>
					<IAText text={langs.totalPoints.toUpperCase()} style={styles.totalPoint}/>
				</View>
				<View style={styles.pointBound}>
					<IAText text={point} style={styles.point}/>
				</View>

			</Animatable.View>
		);
	}

	_renderLeagues = () => {
		return (
			<View>
				<IAText text={langs.myLeague.toUpperCase()} style={styles.myLeague}/>
			</View>
		);
	}

	_renderListLeagueItems = () => {
		var joinedLeagues = this.props.joinedLeagues;
		return (
			<Animated.ScrollView
				horizontal
				showsHorizontalScrollIndicator={false}
				scrollEventThrottle={16}>
				{joinedLeagues.map(item => {
					return (
						<TouchableOpacity key={item.id} onPress={() => {
							this.goToScreen(ScreenNames.LeagueDetailScreen, {item});
						}}>
							<Animatable.View
								animation={"zoomInDown"}
								duration={1500}
								elevation={4}
							>
								<View style={{width: 100}}>
									<View style={styles.imgBound}>
										<FastImage
											style={styles.img}
											source={{
												uri: item.image
													? item.image
													: Constant.MOCKING_DATA.PLACE_HOLDER,
												priority: FastImage.priority.high
											}}
											resizeMode={FastImage.resizeMode.cover}
										/>
									</View>
									<View style={styles.itemContentContainer}>
										<IAText
											style={styles.titleItem}
											text={item.name.toUpperCase()}
											multiLine
											numberOfLines={5}
										/>
									</View>
								</View>
							</Animatable.View>
						</TouchableOpacity>
					);
				})}
			</Animated.ScrollView>
		);
	}

	_renderDataJoinedLeague = () => {
		return (
			<View style={styles.baseContainer}>
				{ this._renderTotalPoints() }
				{ this._renderLeagues() }
				{ this._renderListLeagueItems() }
			</View>
		);
	}

	_renderNoLeagueJoined = () => {
		return (
			<View style={styles.noLeagueJoinedContainer}>
				<IAText text={langs.noLeague} numberOfLines={5} multiLine={true} style={styles.mottoText}/>
				{ this._renderJoinBtnLeague() }
			</View>
		);
	}

	_renderJoinBtnLeague() {
		return (
			<View style={styles.btnLoginContainer}>
					<TouchableOpacity style={styles.btnLogin} onPress={()=>{
						this.goToScreen(ScreenNames.HomeScreen);
					}}>
						<LinearGradient
							colors={Constant.COLOR_BACKGROUND_BTN}
							angle= {1}
							start={{x: 0, y: 0}}
							end={{x: 1, y: 0}}
							location={Constant.LOCATION_BACKGROUND_BTN}
							style={styles.buttonSignInContainer}>
							<IAText style={styles.buttonText} text={langs.joinLeague.toUpperCase()} numberOfLines={5} multiLine={true}/>
						</LinearGradient>
					</TouchableOpacity>
			</View>
		);
	}

  render() {
  	var hasJoined = this.props.joinedLeagues.length > 0;
  	return(
  		<View style={styles.mainContainer}>
  			<StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true}/>
  			<ImageBackground source={images.userProfile} style={styles.imgBackground}>
  				<View style={{marginTop: 11}}>
  					<IAHeader
  						viewLeft={this._renderDrawerIco()}
  						onPressLeft={() => {this._openDrawer();}}
  						viewRight={this._renderSetting()}
  						onPressRight={() => this.goBack()}
  						viewCenter={null}/>
  					<IAText text={langs.title.toUpperCase()} style={styles.title}/>
  			  </View>
  				<View style={styles.waveContainer}>
  					{ this._renderWave() }
  					{ this._renderProfilePicture() }
  				</View>
  			</ImageBackground>
  			<ScrollView
  				contentContainerStyle={{flexGrow: 1}}
  				showsVerticalScrollIndicator={false}
				refreshControl={
					<RefreshControl
						refreshing={false}
						onRefresh={()=>this._onRefresh()}
					/>
				}>
  				<View>
  					{ this._renderBasicUserInfo() }
  					{ hasJoined ? this._renderDataJoinedLeague() : this._renderNoLeagueJoined() }
  				</View>
  			</ScrollView>
  		</View>
  	);
  }
}
const mapStateToProps = state => {
	return {
		joinedLeaguesStatus: state.homeReducer.joinedLeagues.status,
		joinedLeagues: state.homeReducer.joinedLeagues.data,
		joinedLeaguesError: state.homeReducer.joinedLeagues.error,

		userProfileStatus: state.userProfileReducer.userProfile.status,
		userProfile: state.userProfileReducer.userProfile.data,
		userProfileError: state.userProfileReducer.userProfile.error,

		uploadImgGoogleStorageStatus: state.userProfileReducer.uploadImgGoogleStorage.status,
		uploadImgGoogleStorage: state.userProfileReducer.uploadImgGoogleStorage.url,
		uploadImgGoogleStorageError: state.userProfileReducer.uploadImgGoogleStorage.error,

		userProfileUpdateStatus: state.userProfileReducer.userProfileUpdate.status,
		userProfileUpdate: state.userProfileReducer.userProfileUpdate.url,
		userProfileUpdateError: state.userProfileReducer.userProfileUpdate.error,
	};
};
const mapDispatchToProps = dispatch =>
	bindActionCreators({
		getUserProfile,
		uploadImgGoogle,
		uploadUserProfilePicture
	}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);