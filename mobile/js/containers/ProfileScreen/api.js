import Constant from "../../shared/utils/constant/Constant";
import {AxiosFetch} from "../../api/AxiosFetch";
const FormData = require("form-data");

// Get user profile
// eslint-disable-next-line no-empty-pattern
export function getUserProfileFunc({}) {
	// eslint-disable-next-line no-unused-vars
	return new Promise((rs, rj) => {
		AxiosFetch({
			method: "GET",
			url: Constant.URL.API_URL_PATH.user.getUserProfile.url,
			data: null,
			contentType: "application/json",
			onSuccess: data => {
				rs(data);
			},
			onError: error => {
				rj(error);
			},
			hasToken: true
		});
	});
}

// Get url img uploaded
export function uploadUserProfilePicture({filename}) {
	return new Promise((rs, rj) => {
		AxiosFetch({
			method: "PUT",
			url: Constant.URL.API_URL_PATH.user.updateProfilePicture.url,
			contentType: "application/json",
		  	data: {"avatar": filename},
			onSuccess: data => {
				rs(data);
			},
			onError: error => {
				rj(error);
			},
			hasToken: true
		});
	});
}

// Img uploading
export async function uploadImgToGoogleStoreFunc({filename}) {
	var form = new FormData();
	form.append("file", {
		name: new Date().getTime() + ".PNG",
		type: "image/png",
		uri: filename.image,
	});
	return new Promise((rs, rj) => {
		AxiosFetch({
			url: Constant.URL.API_URL_PATH.league.getUrlImageUploaded.url,
			method: "POST",
			data: form,
			contentType: "multipart/form-data",
			onSuccess: data => {
				rs(data);
			},
			onError: error => {
				rj(error);
			},
			hasToken: true
		});
	});
}

export default {
	uploadUserProfilePicture,
	uploadImgToGoogleStoreFunc,
	getUserProfileFunc
};
