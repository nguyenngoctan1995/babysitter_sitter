import {from, of} from "rxjs";
import {Observable} from 'rxjs/Observable';
import {map, catchError, mergeMap, switchMap, concatMap, concat} from "rxjs/operators";
import {ofType} from "redux-observable";
import * as constants from "./constants";
import {getUserProfileFunc, uploadImgToGoogleStoreFunc, uploadUserProfilePicture} from "./api";
import {getUserProfileFailure, getUserProfileSuccess, uploadUserProfilePictureFailure, uploadUserProfilePictureSuccess, uploadImgGoogleSuccess, uploadImgGoogleFailure} from "./actions";
import LogManager from "../../shared/utils/logging/LogManager";
import { database } from "react-native-firebase";

export const loadingGetUserProfile = () => ({type: constants.GET_USER_PROFILE});

export const loadingGetUserProfileEpic = action$ => {
	return action$.pipe(
		ofType(constants.GET_USER_PROFILE),
		// eslint-disable-next-line no-unused-vars
		mergeMap(action =>
			from(getUserProfileFunc({})).pipe(
				map(result => getUserProfileSuccess({result})),
				catchError(error => of(getUserProfileFailure({error})))
			)
		)
	);
};

export const loadingUploadImgGoogle = () => ({type: constants.UPLOAD_IMG_GOOGLE});

export const loadingUploadImgGoogleEpic = action$ => {
	return action$.pipe(
		ofType(constants.UPLOAD_IMG_GOOGLE),
		// eslint-disable-next-line no-unused-vars
		concatMap(action => {
			return from(uploadImgToGoogleStoreFunc({filename: action.payload.filename})).pipe(
				map(data => uploadUserProfilePicture({filename: data.data.data.url})),
				map(data => uploadImgGoogleSuccess(data.data.data.url)),
				catchError(error => of(getUserProfileFailure({error}))),
				catchError( error => of(uploadImgGoogleFailure({error}))) 
			)
		}))
};
export const loadingUploadUserProfilePicture = () => ({type: constants.UPLOAD_USER_PROFILE_PICTURE});

export const loadingUploadUserProfilePictureEpic = action$ => {
	return action$.pipe(
		ofType(constants.UPLOAD_USER_PROFILE_PICTURE),
		// eslint-disable-next-line no-unused-vars
		mergeMap(action =>
			from(uploadUserProfilePicture({filename: action.payload.filename})).pipe(
				map(result => uploadUserProfilePictureSuccess({result})),
				catchError(error => of(uploadUserProfilePictureFailure({error})))
			)
		)
	);
};