/* eslint-disable react/display-name */
import {
	createStackNavigator,
	createAppContainer,
	createSwitchNavigator,
	createDrawerNavigator,
	createBottomTabNavigator,
} from "react-navigation";
import React from "react";
import {View} from "react-native";
import SplashScreen from "../containers/SplashScreen/index";
import LoginScreen from "../containers/LoginScreen/index";
import ProfileScreen from "../containers/ProfileScreen/index";
import HomeScreen from "../containers/HomeScreen/index";
import ForgotPasswordScreen from "../containers/ForgotPasswordScreen/index";
import ChangePasswordScreen from "../containers/ChangePasswordScreen/index";
import {colors} from "../shared/utils/colors/colors";
import TabbarComponentCustom from "./TabbarComponentCustom";
import Drawer from "../route/Drawer";
import {ScreenWidth} from "../shared/utils/dimension/Divices";
import icons from "../shared/utils/icons/icons";
import NavigationService from "../shared/utils/navigation/NavigationService";

// Create bottom tab for navigator
const AppBottomTabNavigator = createBottomTabNavigator(
	{
		HomeScreen: {
			screen: HomeScreen,
			navigationOptions: {
				tabBarIcon: ({focused}) => {
					return (
						<View>
							{focused ? icons.tabbar.home.IC_PRESSED_HOME : icons.tabbar.home.IC_NORMAL_HOME}
						</View>
					);
				}
			}
		},		
		ProfileScreen: {
			screen: ProfileScreen,
			navigationOptions: {
				tabBarIcon: ({focused}) => {
					return (
						<View>
							{focused ? icons.tabbar.setting.IC_PRESSED_SETTING : icons.tabbar.setting.IC_NORMAL_SETTING}
						</View>
					);
				}
			}
		},
	},
	{
		tabBarOptions: {
			showLabel: false, // Define if title in bottom will be display
			activeTintColor: colors.pink,
			inactiveTintColor: colors.black
			// statusBarHidden: false,
		},
		tabBarComponent: TabbarComponentCustom
	}
);



export default class AppContainer extends React.Component {
	render() {
		const Navigator = createDrawerNavigator(
			{MainScreenNavigator: {screen: AppBottomTabNavigator}},
			{
				contentComponent: (props) => {
					return (
						<Drawer {...props}/>
					);
				},
				drawerWidth: ScreenWidth,
			}
		);
		// SwitchNavigator for using the multiple stack in the same route
		const AppSwitchNavigator = createSwitchNavigator({
			SplashScreen: {screen: SplashScreen},
			WelcomeScreen: {screen: WelcomeScreen},
			HomeScreen: {screen: Navigator},
		});
		// ScreenNavigator for separately each screen into stack one
		const MainScreenNavigator = createStackNavigator(
			{
				MainScreen: {screen: AppSwitchNavigator},
				LoginScreen: {screen: LoginScreen},
				SignUpScreen: {screen: SignUpScreen},
				ForgotPasswordScreen: {screen: ForgotPasswordScreen},
				ChangePasswordScreen: {screen: ChangePasswordScreen},
			},
			{
				mode: "modal",
				headerMode: "none",
				transparentCard: true,
			}
		);
		const App = createAppContainer(MainScreenNavigator);
		return <App ref={navigatorRef => {
          			NavigationService.setTopLevelNavigator(navigatorRef);
				}}
				{...this.props} />;
	}

}
