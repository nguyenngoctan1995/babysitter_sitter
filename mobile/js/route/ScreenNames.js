export const ScreenNames = {
	SplashScreen: "SplashScreen",
	LoginScreen: "LoginScreen",
	ProfileScreen: "ProfileScreen",
	HomeScreen: "HomeScreen",
	MainTabScreen: "MainTabScreen",
	SignUpScreen: "SignUpScreen",
	ForgotPasswordScreen: "ForgotPasswordScreen",
	ChangePasswordScreen: "ChangePasswordScreen",
};

export default {
	ScreenNames
};
