module.exports = {
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  printWidth: 100,
  singleQuote: false,
  tabWidth: 4,
  useTabs: true,
  bracketSpacing: false
};
