# Babysisters Blog
Welcome to BabySitter's project!
Please jump onto it for installation.
# Get started!
* yarn install 

* react-native link

* cd ios & pod install


# Branches:
* __master__: _Only in the latest release to customer for launching.*

* __release__: _This will be the most important branch for building new version for testing to __CUSTOMER__._
* __staging__: _This will be the most important branch for building new version for testing to __TESTER__._
* __features__: _Here, you can go anything new._
* __fixbugs__: _Oops, some bugs have to be fixed._
* __develop__: _Please, do note that: everything in features, fixbugs branches will be checked out from here._

# Now move on!
